# First the base image that the docker is using will be defined
FROM ubuntu:18.04
# Conda manages our python enviroment
# We need to set some enviroment variables that allow
# our docker to find the installation
ENV PATH="/home/mom5/miniconda3/bin:${PATH}"
ARG PATH="/home/mom5/miniconda3/bin:${PATH}"
# Next we will update the system and install all necessary tools
# such as netCDF (data output of the ocean model)
# openMPI (parallel computing)
# vim (text editor)
# and gcc (compiler)
RUN apt-get update && apt-get install -y  git \
                        tcsh \
                        pkg-config \
                        gfortran \
                        netcdf-bin \
                        libnetcdf-dev \
                        libnetcdff-dev \
                        openmpi-bin \
                        libopenmpi-dev \
                        libnetcdff-dev \
                        vim \
                        wget
# Add a user to the system called mom5
RUN useradd -ms /bin/bash mom5
# Copy model code to our virtual docker image and change the ownership to mom5 (user)
COPY --chown=mom5:mom5 MOM_code /home/mom5/MOM_code
# Remove some files that may cause problemes
RUN rm -rf /var/lib/apt/lists/*
# Switch from root to user MOM5
USER mom5
# Say that work directory is /home/mom5
WORKDIR /home/mom5/
# Change directy to MOM source code and compile the model
RUN cd /home/mom5/MOM_code && ./build
# This might be obsolete
WORKDIR /home/mom5
# Install conda packaging manager
RUN wget \
    https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && mkdir .conda \
    && bash Miniconda3-latest-Linux-x86_64.sh -b \
    && rm -f Miniconda3-latest-Linux-x86_64.sh 
# Initialize conda
RUN conda --version && conda init
# Install necessary python packages for for our tasks
RUN conda install -c conda-forge -y pyyaml pandas xarray netcdf4 jupyterlab matplotlib nc-time-axis
