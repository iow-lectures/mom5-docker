# Docker MOM-5

## IMPORTANT NOTE

Do you have git installed?

``` IF YOU ARE USING WINDOWS USE for clonning the directory```

`git clone https://git.io-warnemuende.de/boergel/mom5-docker.git --config core.autocrlf=input`

This is AFAIK the only way to avoid end of line reading errors.

## What are you going to do?

This repository creates a docker image that compiles MOM5 using Ubuntu 18.04. The docker image can be distributed to students or can be used as a first introductin to MOM5. 

This image can be used on your personal computer. However, docker allows multi-processing, hence, computing on the IOW-servers should be also possible.

In the following you will find a step by step guide to run your own first MOM5 ocean only (MOM-SIS) simulation.

## Step-by-step guide

### Install docker on your local machine

Docker Engine is supported on `x86_64` (or `amd64`), `armhf`, and `arm64` architectures.

It is advised to follow the description give on the offical [docker homepage](https://docs.docker.com/engine/install/)

### Download the forcing data

The forcing data can be download from the butto below. It will open a website called zenodo. zenodo can be used to share data.

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.4436257.svg)](https://doi.org/10.5281/zenodo.4436257)

The access is restricted and you need to ask for permission. Once you have been given access download the data and put them into `./MOM_forcing`. 

### What is a dockerfile

Probably you are already familiar with Virtual Machine (`VMs`). Docker is similiar. The key difference that docker is is a **container**  based technology and containers are just user space of the operating system.

The dockerfile is giving a description on how to build the container. Please have a look at the dockerfile and see the comments.

```docker
FROM ubuntu:18.04
ENV PATH="/home/mom5/miniconda3/bin:${PATH}"
ARG PATH="/home/mom5/miniconda3/bin:${PATH}"

RUN apt-get update && apt-get install -y  git \
                        tcsh \
                        pkg-config \
                        gfortran \
                        netcdf-bin \
                        libnetcdf-dev \
                        libnetcdff-dev \
                        openmpi-bin \
                        libopenmpi-dev \
                        libnetcdff-dev \
                        vim \
                        wget

RUN useradd -ms /bin/bash mom5

[...]
```
### Building the docker image

To build the image you need to start your terminal. The git bash will not work. On a unix system (Linux/MacOS) you can use the default terminal. On Windows you need to use the PowerShell.

Inside the terminal navigate to the mom5-docker folder and execute

```bash
docker build -t mom5:0.2 .
```

**IMPORTANT**: If you are using a system like Mac M1 silicon, you need to change the line above to:

```
docker build -t mom5:0.2 --platform=linux/arm64,linux/amd64 .
```

to build the docker image. This will take a while since everything will be downloaded and setup for you. 

## Compiling the model

The dockerfile will automatically compile our ocean model. The compilation time varies from system to system and will be performed automatically once you build the docker image.

The corresponding line in the dockerfile is:

```docker 
RUN cd /home/mom5/MOM_code && ./build
```

Note that MOM consists of more than 100.000 lines of code that need to be compiled into an executable. If you are interested find [here](https://mom-ocean.github.io/assets/pdfs/MOM5_manual.pdf) the model documentation. Once the compilation completes, an executable
is placed in the folder `/home/mom5/MOM_code/exec/gfortran/MOM_SIS` called `fms_MOM_SIS.x`. This is the executable that runs the model.


### Run the docker

Before executing the line below please have a look at it's content:

```bash 
docker run -v $(PWD)/model_setup:/home/mom5/MOM_setup -v $(PWD)/MOM_forcing:/home/mom5/MOM_forcing -v $(PWD)/model_simulations:/home/mom5/model_simulations -p 8888:8888 -it mom5:0.2 bash
```

**Note**: Please note that depending on your operating system you might need to change the brackets ( ) to curly brackets { }.

`-v` mounts the corresponding folder into the virtual machine. In contrast to copying the folder to your docker image, we avoid that the docker image gets to big.

Now execute the line.

You should now see something like

```bash
mom5@08a244e1e168:~$ 
```

You successfully started the docker image and are now inside the virtual machine. In our case an Ubuntu 18.04 OS.



**Note:** if you get errors about while mounting the folders some possible solutions

1. If you are using cmd.exe on windows, if available use powershell. Git bash will not work.  
2. replace `$(PWD)` in that command above with `%cd%` if you need to use cmd.exe
3. try changing `$(PWD)` to `$pwd`


## Start the ocean model

We will now navigate inside our virtual machine. Copy the previously compiled executable to the MOM_setup folder

```bash
cp /home/mom5/MOM_code/exec/gfortran/MOM_SIS/fms_MOM_SIS.x /home/mom5/MOM_setup/binary/.
```

Inside `/home/mom5/MOM_setup` you will find the following structure
```bash
├── binary
│   └── fms_MOM_SIS.x
├── create_model_setup.py
├── firstlog
├── forcing_file.yaml
├── init
│   ├── ocean_ergom.res.nc
│   ├── ocean_temp_salt.res.nc
│   └── sst_ice_clim.nc
├── input
│   ├── data_table
│   ├── data_table~
│   ├── diag_table
│   ├── diag_table~
│   ├── field_table
│   ├── grid_spec.nc
│   ├── input.nml
│   ├── input.nml~
│   ├── postprocessing.sh
│   └── roughness_cdbot.nc
├── job_driver
├── job_reference_run50_init
├── job_submit_git
├── job_v02_r01
├── log
├── log2
├── logfile
├── logtest
├── __pycache__
│   ├── create_submit_python.cpython-37.pyc
│   └── create_submit_python.cpython-38.pyc
├── RUNYEAR01
├── test
└── tst
```

The file `create_model_setup.py` will handle everything for you. 

Just type 

```bash
 python create_model_setup.py -h
 ```
 and follow the instructions. Note that `-begin` needs to be `1961` and `-end` `1962` for our example.

The folder `input/` and `init/` represent our cooking book for the model and the initial conditions, respectively. `forcing_file.yaml` links the downloaded forcing data into the defined setup. If used correctly you will end up with a folder `/home/mom5/model_simulations/{your simulation name}`.

In summary, you need to execute:

```bash
python create_model_setup.py -n {your simulation name} -nforcing forcing_file.yaml -inputfolder input -begin 1961 -end 1962 
```

Go to that directory and go to `1961/` and type `./fms_MOM_SIS.x` to run the model.

This will take a while now.

After the end of the simulation you should see the files `ice_day.nc` and 
`ocean_day3d.nc` which we will analyze in the following.

# After the lecture:

If you finished your model simulation already, you should check the model_simulations folder on your local machine (inside your mom5-docker repository). It should contain your model run.

If you want to start your docker image again, please use your terminal (for windows power shell is best) and direct yourself using to the mom5-docker folder. Once you are in that repository execute  to start the docker.

If you want to analyze your data, you should start now your jupyter notebook. Please navigate to your simulation directory using `cd` (in the docker) and the year 1961. You should now see something like `~/model_simulations/mariesostseelauf/1961`

Now please enter: 

`jupyter lab --ip=0.0.0.0 .`

This will start a jupyter server. You should see in your terminal something like:

`http://127.0.0.1:8888/lab?token=207782a53ea615d3cef55720b20cfaa8911496e9f8ec323d`

Open now the web browser on your local machine and open that URL.

You should see now something like this

![](figures/jupyterlab.png)

Click on the python3 notebook.

You should see an notebook environment now and a notebook called Untitled.ipynb in the left column where all files are listed.

The notebook is also stored on you local machine. 

Analyze the data and respond to the two small tasks inside the notebook template.
## Running the model longer

If you want to run your model longer:

Remember that also your model setup is stored on you local disk. Direct to the folder and open year 1961 again. Use a text editor of you choice to open the `input.nml` file.

For me it is stored here: `C:\Users\lokadmin\Documents\work\mom5-docker\model_simulations\mariesostseelauf\1961\input.nml`

Set `days` to 0 and `months=1`. Then the model will run for 1 month. The maximum here is **11** months, since we have atmospheric forcing only available for one year.

For the homework 11 months of simulation would be best, but are **not** needed. Your notebook will likely take a while to simulate such a long period. So my advise would be to run the simulation over night.

Start your docker again and start the model there using `./fms_MOM_SIS.x` inside your simulation folder.

It will override your previouslcy create data. 

# Additional information

### Multi processing

If you want to use multiple cores use 

```bash
mpirun -np <your number of cores> ./fms_MOM_SIS.x
```

**Important**: using multiple core requires to add a `layout` to `&ocean_model_nml` and `&ice_model_nml` in the file `input.nml`.


For example if you want to use 4 cores:

```
#cat input.nml

&ice_model_nml
[...]
layout = 2,2
\

&ocen_model_nml
[...]
layout = 2,2
\
```

### docker-compose.yml

For largers applications the docker-compose file can give some structure on how to build the application. In this case it might be a little bit over the top.

```docker
services:
  MOM:
    build: .
    image: mom5:0.2
```

To install docker-compose run on Linux

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

and check with 

```bash
docker-compose --version
```

if the installation worked.

For more details see [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)