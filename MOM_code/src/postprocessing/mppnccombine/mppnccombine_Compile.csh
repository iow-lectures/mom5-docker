#!/bin/csh -v

module load module load intel/18.0.6
module load netcdf/intel/4.7.3

module list
# hack weil pkg-config nicht funktioniert, variable nicht gesetzt
# hier: netcdf-cxx4.pc  netcdf-fortran.pc  netcdf.pc
#setenv PKG_CONFIG_PATH '/sw/dataformats/netcdf/4.6.1/skl/intel.18.0.3/lib/pkgconfig'
echo $PKG_CONFIG_PATH

make -f makefile_icc mppnccombine

