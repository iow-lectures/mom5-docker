import argparse
from pathlib import Path
import shutil
import os
import yaml
import glob

from os import symlink
from stat import *
import pandas as pd
import xarray as xr


import logging

parser = argparse.ArgumentParser()
parser.add_argument("-n", "--name", required=True,
                    help="Name of model experiment; defines folder name")
parser.add_argument('-nforcing', '--nameforcingfile', required=True, help="Name of forcing file")
parser.add_argument("-inputfolder", required=True, help="Folder containing setup dependent files")
parser.add_argument("-logname", help = "name of log file created; default 'logfile'")

period = parser.add_argument_group('Time range', 'Defines simulation period')
period.add_argument('-begin', required = True, help='Begin of simulation')
period.add_argument('-end', required= True, help='End of simulation')
args = parser.parse_args()


if args.logname:
    logging.basicConfig(level=logging.DEBUG, filename="{}".format(args.logname), filemode="a+",
                                format="%(asctime)-15s %(levelname)-8s %(message)s")
else:
    logging.basicConfig(level=logging.DEBUG, filename="logfile", filemode="a+",
                                format="%(asctime)-15s %(levelname)-8s %(message)s")
if args.name:
    logging.info("Creating model setup ../model_simulations/{}".format(args.name))
    Path("../model_simulations/{}".format(args.name)).mkdir(parents=True, exist_ok=True)

logging.info("Creating folders for years {} to {} with INPUT/ and RESTART/".format(args.begin, args.end))
for year in range(int(args.begin), int(args.end)):
    dirpath = "../model_simulations/{}/{}".format(args.name, year)
    Path(dirpath).mkdir(exist_ok=False)
    Path(dirpath + "/INPUT").mkdir(exist_ok=True)
    Path(dirpath + "/RESTART").mkdir(exist_ok=True)
    
    shutil.copyfile("binary/fms_MOM_SIS.x", dirpath + "/fms_MOM_SIS.x")

    for filename in glob.glob("init/*.nc"):
        logging.info("Copying from initfolder ... {}".format(filename))
        shutil.copy(filename, dirpath + "/INPUT/.")

    for filename in glob.glob(args.inputfolder + "/*"):
        logging.info("Copying {} ...".format(filename))
        try:
            shutil.copy(filename, dirpath + "/.")
        except:
            logging.info("Skipping directory in {}".format(args.inputfolder))
 
    for mask_table in glob.glob(dirpath + "/mask_table.*"):
        logging.info("Moving mask table to INPUT/.")
        shutil.move(mask_table, dirpath + "/INPUT/.")

    logging.info("Moving roughness_cdbot.nc and grid_spec.nc to INPUT/.")
    shutil.move(dirpath + "/roughness_cdbot.nc", dirpath + "/INPUT/.")
    shutil.move(dirpath + "/grid_spec.nc", dirpath + "/INPUT/.")
    
    logging.info("Changing file permissions")
    for filename in glob.glob(dirpath + "/*.*"):
        os.chmod(filename, 0o664)
        
    os.chmod(dirpath + "/fms_MOM_SIS.x", 0o755)
    logging.info("Linking forcing ...") 
    with open(args.nameforcingfile) as file:
            forcingfile = yaml.load(file, Loader=yaml.FullLoader)
    for keys, forcingelement in forcingfile.items():
        logging.info("{} to INPUT/{}".format(forcingelement['path'].replace("$YEAR", str(year)), forcingelement["name"]))
        os.symlink(forcingelement['path'].replace("$YEAR", str(year)), dirpath + "/INPUT/{}".format(forcingelement['name']))


        ### The following procedure checks whether the defined variable names in the data_table
        ### is in the defined forcing in args.nameforcingfile
        data = pd.read_table(dirpath + "/data_table", skiprows = 4, delimiter=",", header = None).dropna()
        data = data[data[3].str.contains(forcingelement["name"])]
        if data is not None:
            for line in range(len(data)):
                try:
                    _ = xr.open_dataset(forcingelement['path'].replace("$YEAR", str(year)), decode_times = False)[data.iloc[line][2].replace('"','').strip()]
                except Exception as e:
                    logging.warning("{} with variable {} could not be opened".format(forcingelement['path'].replace("$YEAR", str(year)), data.iloc[line][2].replace('"','').strip()))
        ### end validation of forcing 

    logging.info("Writing experiment name to diag_table")
    diag_table = pd.read_table(args.inputfolder + "/diag_table", skiprows = 2)

    with open(dirpath + "/diag_table") as f: 
        lines = f.readlines()
    
    lines[0] = '"' + str(args.name) + '"\n'
    lines[1] = str(args.begin) + " 2 1 0 0 0\n"

    with open(dirpath + "/diag_table", 'w') as f: 
        f.writelines(lines)
        
#     logging.info("Creating job script") 
#     run = jobscript(name = str(args.name),
#                     cluster = "HLRN",
#                     walltime = "12:00:00",
#                     nodes = 20,
#                     ey = args.end,
#                     number_of_loops = 5,
#                     projectname = str(args.name),
#                     folder = "",
#                     ppn = 5)
# run.writeHeader()
# run.writeLoop()
# run.fileclose()
# shutil.move("{}".format(args.name),  dirpath + "/../.")
# os.chmod(dirpath + "/../{}".format(args.name), 0o755)
shutil.copy("job_driver", dirpath + "/../.")
shutil.copy("job_v02_r01", dirpath + "/../job_{}".format(args.name))

with open(dirpath + "/../job_driver") as f: 
    lines = f.readlines()
    lines[3] = "job=job_{}\n".format(args.name)
with open(dirpath + "/../job_driver", 'w') as f: 
     f.writelines(lines)
        
with open(dirpath + "/../job_{}".format(args.name)) as f: 
    lines = f.readlines()
    lines[27] = "EY={}\n".format(args.end)
with open(dirpath + "/../job_{}".format(args.name), 'w') as f: 
     f.writelines(lines)
          
with open(dirpath +"/../RUNYEAR01", 'w') as f:
    f.write(args.begin)
print("Remember to set months to 11 in input nml for {}".format(args.begin))