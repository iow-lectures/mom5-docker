# Create MOM Setup from scratch

The idea is that `create_model_setup.py` is the only file that needs to be used for creating a MOM setup.

## Folder structure

* binary/
    * fms_MOM_SIS.x
* create_model_setup.py
* create_submit_python.py
* forcing_file.yaml
* init/
    * ocean_ergom.res.nc
    * ocean_temp_salt.res.nc
    * sst_ice_clim.nc
* input/
    * data_table
    * diag_table
    * field_table
    * grid_spec.nc
    * input.nml   
    * mask_table.121.14x17
    * postprocessing.sh   
    * roughness_cdbot.nc

## Usage
