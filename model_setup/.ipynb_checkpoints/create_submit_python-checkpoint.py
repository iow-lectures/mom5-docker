import numpy as np
import argparse

class jobscript():

    def __init__(self,
                 name,
                cluster,
                walltime,
                nodes,
                ey,
                folder,
                ppn,
                number_of_loops,
                projectname=None):
        self.name = name
        self.cluster = cluster
        self.walltime = walltime
        self.nodes = nodes
        self.ppn = ppn
        self.folder = folder
        self.bin = "fms_MOM_SIS.x"
        self.EY = ey
        if projectname:
            self.project = projectname
        self.loops = number_of_loops
        self.f = open(name, 'w')


    def writeHeader(self):
        self.f.write("#!/bin/bash\n")
        if self.cluster == 'hlrn' or self.cluster == 'HLRN':
            self.f.write('#PBS -N {} \n'.format(self.name)) 
            self.f.write('#PBS -l nodes={}:ppn{}\n'.format(self.nodes,
                        self.ppn))
            self.f.write('#PBS -l walltime={}\n'.format(self.walltime))
            self.f.write("#PBS -j oe \n" + '#PBS -l feature=mpp\n')
            self.f.write("#PBS -A {} \n".format(self.project))
            self.f.write("EXPDIR={}\n".format(self.folder))
            self.f.write("BIN={}\n".format(self.bin))
            self.f.write("COUNTER=RUNYEAR01\n")
            self.f.write("EY={}\n".format(self.EY))

    def writeLoop(self):
        self.f.write("YEAR=`cat $COUNTER`\n")
        self.f.write("for i in {{1..{}}}\n".format(self.loops))
        self.f.write("do\n" +
              "  if [ $YEAR -le $EY ]; then\n" +
              "    cd ./${EXPDIR}${YEAR}\n" +
              "    let 'YEAR = YEAR + 1'\n" +
              "    echo $YEAR >  ../$COUNTER\n" +
              "    echo $YEAR\n" +
              "    date > start\n" +
              "    aprun  -n 117 ./$BIN > ergom.out\n" +
              "    date > end\n" +
              "  fi\n"+
              "  if grep NaN  ergom.out > /dev/null ; then\n"+
              "    let 'YEAR = EY + 1'\n" +
              "  fi\n" +
              "  if grep MPP_STACK ergom.out > /dev/null ; then\n"+
              "    echo okay\n" +
              "  else\n"+
              "    let 'YEAR = EY + 1'\n"+
              "  fi\n" +
              "  if [ $YEAR -le $EY ]; then\n" +
              "    cp RESTART/* ../${EXPDIR}${YEAR}/INPUT\n" + 
              "    echo $YEAR data copied to concecutive directory\n" +
              "  fi\n" +
              "done\n")


    def fileclose(self):
        if self.f:
            self.f.close()
            self.f = None

