#!/bin/bash
# at HLRN
d="/gfs2/work/mvkthne2/mom_512_iow/src/postprocessing/mppnccombine/"
#d="/work/thomas/ModelCode/MOM511/mom//src/postprocessing/mppnccombine/"
${d}mppnccombine -n4 -d 3 -m        -r gridinfo.nc
${d}mppnccombine -n4 -d 3 -m -k 150 -r atmos_day.nc
${d}mppnccombine -n4 -d 3 -m -k 150 -r ice_day.nc
${d}mppnccombine -n4 -d 3 -m -k 150 -r ocean_day2d.nc
${d}mppnccombine -n4 -d 3 -m -k 150 -r ocean_day3d.nc
#${d}mppnccombine -n4 -d 3 -m -k 732 -r ocean_STO.nc
${d}mppnccombine -n4 -d 3 -m -k 150 -r ocean_trps.nc
${d}mppnccombine -n4 -d 3 -m -k 150 -r ergom_flux3d.nc
${d}mppnccombine -n4 -d 3 -m -k 150 -r ergom_flux3d.nc
${d}mppnccombine -n4 -d 3 -m -k 150 -r ergom_flux_surf.nc
${d}mppnccombine -n4 -d 3 -m -k 150 -r ergom_flux_sed.nc



